const styles = () => ({
    container: {
        marginTop: '3.5rem',
    },
    form: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        margin: 0,
    },
    formulario: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBottom: '1rem',
    },
});

export default styles