import React, { useState, useEffect } from 'react';
import Condiciones from '../condiciones';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab, Typography, Box, IconButton, TextField } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import LocationOnIcon from '@material-ui/icons/LocationOn';

import styles from './style';

const ColorButton = withStyles((theme) => ({
    root: {
      color: '#0a4399',
    },
}))(IconButton);

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
}
  
TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const APIkey = '521bd8e998d8f8bdf4c4a75dc2352e7d';

function Clima(props) {
    let [error, setError] = useState(null);
    let [isLoaded, setIsLoaded] = useState(false);
    let [response, setResponse] = useState({});
    let [city, setCity] = useState('');
    let [location, setLocation] = useState({});

    const { classes } = props;

    function getForecast(e) {

        e.preventDefault();

        if (city.length === 0) {
            return setError(true);
        }

        // Clear state in preparation for new data
        setError(false);
        setResponse({});
    
        setIsLoaded(false);

        const uriEncodedCity = encodeURIComponent(city);

        fetch(`https://api.openweathermap.org/geo/1.0/direct?q=${uriEncodedCity}&limit=1&appid=${APIkey}`)
            .then(location => location.json())
            .then(
            (location) => {
                setLocation(location);
                fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${location[0].lat}&lon=${location[0].lon}&units=metric&lang=es&exclude=minutely&appid=${APIkey}`)
                    .then(response => response.json())
                    .then(
                        (response) => {
                            setResponse(response);
                            setIsLoaded(true);
                    },
                    )
                    .catch(err => {
                        setError(true);
                        setIsLoaded(true);
                        console.log(err.message);
                    }); 
            },
            )
            .catch(err => {
                setError(true);
                setIsLoaded(true);
                console.log(err.message);
        });
    }

    const getForecastCurrentLocation = () => {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${position.coords.latitude}&lon=${position.coords.longitude}&units=metric&lang=es&exclude=minutely&appid=${APIkey}`)
                    .then(response => response.json())
                    .then(
                        (response) => {
                            fetch(`https://api.openweathermap.org/geo/1.0/reverse?lat=${position.coords.latitude}&lon=${position.coords.longitude}&limit=1&appid=${APIkey}`)
                                .then(location => location.json())
                                .then(
                                (location) => {
                                    setLocation(location);
                                    setResponse(response);
                                    setIsLoaded(true);
                                },
                                )
                                .catch(err => {
                                    setError(true);
                                    setIsLoaded(true);
                                    console.log(err.message);
                            });
                        },
                    // Nota: es importante manejar errores aquí y no en 
                    // un bloque catch() para que no interceptemos errores
                    // de errores reales en los componentes.
                        (error) => {
                            setIsLoaded(true);
                            setError(error);
                        },
                    )
            });
        } else {
            window.alert("Este navegador no soporta geolocalización.");
        }
    };


    useEffect(() => {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${position.coords.latitude}&lon=${position.coords.longitude}&units=metric&lang=es&exclude=minutely&appid=${APIkey}`)
                    .then(response => response.json())
                    .then(
                        (response) => {
                            fetch(`https://api.openweathermap.org/geo/1.0/reverse?lat=${position.coords.latitude}&lon=${position.coords.longitude}&limit=1&appid=${APIkey}`)
                                .then(location => location.json())
                                .then(
                                (location) => {
                                    setLocation(location);
                                    setResponse(response);
                                    setIsLoaded(true);
                                },
                                )
                                .catch(err => {
                                    setError(true);
                                    setIsLoaded(true);
                                    console.log(err.message);
                            });
                        },
                    // Nota: es importante manejar errores aquí y no en 
                    // un bloque catch() para que no interceptemos errores
                    // de errores reales en los componentes.
                        (error) => {
                            setIsLoaded(true);
                            setError(error);
                        },
                    )
            });
        } else {
            window.alert("Este navegador no soporta geolocalización.");
        }
    }, [])

        

    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    return (
        <div className={classes.container}>
            <div className={classes.formulario}>
                <form onSubmit={getForecast} className={classes.form}>
                    <TextField 
                        id="outlined-search" 
                        label="Ingrese la ciudad" 
                        type="search" 
                        variant="outlined" 
                        value={city}
                        onChange={(e) => setCity(e.target.value)}
                    />
                    <ColorButton aria-label="enviar" type="submit">
                        <SendIcon />
                    </ColorButton>
                </form>
                <ColorButton onClick={getForecastCurrentLocation}>
                        <LocationOnIcon />
                </ColorButton>
            </div>
            <div>
                <AppBar position="static" color="default">
                    <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                    >
                    <Tab label="Actual" {...a11yProps(0)} />
                    <Tab label="Semana" {...a11yProps(1)} />
                    </Tabs>
                </AppBar>
                <SwipeableViews
                    axis={'x'}
                    index={value}
                    onChangeIndex={handleChangeIndex}
                >
                    <TabPanel value={value} index={0}>               
                        <Condiciones
                            response={response}
                            location={location}
                            error={error}
                            loading={isLoaded}
                            tab={0}
                        />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <Condiciones
                            response={response}
                            location={location}
                            error={error}
                            loading={isLoaded}
                            tab={1}
                        />
                    </TabPanel>
                </SwipeableViews>
            </div>
        </div>
    );
}



export default withStyles(styles)(Clima);