import Clima from './clima';
import Condiciones from './condiciones';
import Layout from './layout';
import Header from './header';
import SEO from './seo';
import Footer from './footer';

export {
    Clima,
    Condiciones,
    Layout,
    Header,
    SEO,
    Footer,
};
