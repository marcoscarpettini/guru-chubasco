import React from 'react';
import { Helmet } from 'react-helmet';

import Favicon from '../../images/favicon.ico';

function SEO(props) {
    return (
        <>
            <Helmet titleTemplate='%s | Guru Chubasco'>
                <html lang="es"/>
                <title>{props.title}</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                <meta name="description" content="Esta es una página web de pronostico del tiempo y visualización de las distintas variables metereológicas de cualquier lugar del mundo." />
                <meta name="og:description" content="Esta es una página web de pronostico del tiempo y visualización de las distintas variables metereológicas de cualquier lugar del mundo." />
                <meta name="og:title" content={props.title} />
                <meta name="author" content="Marco Scarpettini" />
                <meta name="og:type" content="website" />
                <link rel="shortcut icon" href={ Favicon } type="image/x-icon"></link>
                <link rel="icon" href={ Favicon } type="image/x-icon"></link>
            </Helmet>
        </>
    );
}

export default SEO