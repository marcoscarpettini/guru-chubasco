const styles = () => ({
    root: {
        maxWidth: 345,
    },
    media: {
        objectFit: 'contain',
        maxHeight: 140,
        marginBottom: 0,
    },
    title: {
        marginTop: 0,
        marginBottom: '0.35em',
        color: '#6c757d',
    },
    temp: {
        marginTop: 0,
        marginBottom: '0.3em',
        fontWeight: 'lighter',
    },
    tempMax: {
        marginTop: 0,
        marginBottom: '0.3em',
        fontWeight: 'lighter',
        color: '#CB2C2A',
    },
    tempMin: {
        marginTop: 0,
        marginBottom: '0.3em',
        fontWeight: 'lighter',
        color: '#0a4399',
    },
    text: {
        color: '#6c757d',
        margin: '0 1rem',
    },
    textDescription: {
        color: '#6c757d',
        margin: '0 1rem',
    },
    paper: {
        display: 'inline-block', 
        whiteSpace: 'normal',
        margin: 'auto 1rem !important',
        maxWidth: '10rem',
        verticalAlign: 'middle',
    },
    papers: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    scrollbar: {
        display: 'none',
        '@media (min-width: 600px)' : {
            display: 'flex',
            minHeight: '15rem',
            whiteSpace: 'nowrap',
            overflowX: 'scroll',
            overflowY: 'hidden',
        }
    },
    pageContainer: {
        display: 'flex',
        flexDirection: 'row',
        minHeight: '100%',
        minWidth: '100%',
        transition: 'filter .5s, opacity .5s',
        boxSizing: 'border-box',
    },
    displayMobile: {
        display: 'flex',
        flexDirection: 'column',
        '@media (min-width: 600px)' : {
            display: 'none'
        }
    },
    stepper: {
        maxWidth: 400,
        flexGrow: 1,
        marginTop: '1rem',
    },
    imageWind: {
        maxHeight: '3rem',
        margin: 0,
    },
    imageWindHourly: {
        maxHeight: '2rem',
        margin: 0,
    },
    displayDesktop: {
        display: 'none',
        '@media (min-width: 600px)' : {
            display: 'flex'
        }
    },
});

export default styles