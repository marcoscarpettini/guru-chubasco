import React, { useState } from 'react';
import { Card, CardActionArea, CardContent, CardMedia, Paper, Grid, MobileStepper, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import WeatherIcon from 'react-open-weather-icons';
import { Scrollbars } from 'react-custom-scrollbars';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import North from '../../images/wind/north.png';
import East from '../../images/wind/east.png';
import South from '../../images/wind/south.png';
import West from '../../images/wind/west.png';
import NorthEast from '../../images/wind/north-east.png';
import NorthWest from '../../images/wind/north-west.png';
import SouthEast from '../../images/wind/south-east.png';
import SouthWest from '../../images/wind/south-west.png';

import styles from './style';

const Condiciones = (props) => {

    const [activeStep, setActiveStep] = useState(0);
    
    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };
    
    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    var weekday=new Array(7);
    weekday[1]="Lunes";
    weekday[2]="Martes";
    weekday[3]="Miércoles";
    weekday[4]="Jueves";
    weekday[5]="Viernes";
    weekday[6]="Sábado";
    weekday[0]="Domingo";

    const renderThumb = ({ style, ...props }) => {
        const thumbStyle = {
            borderRadius: 6,
            backgroundColor: '#0a4399',
            float: 'left',
        };
        return <div style={{ ...style, ...thumbStyle }} {...props} />;
    };
      
    const CustomScrollbars = props => (
        <Scrollbars
            renderThumbHorizontal={renderThumb}
            renderThumbVertical={renderThumb}
            {...props}
        />
    );

    function  toIcon(degree){
        if ((degree>337.5 && degree<=360)|| (degree>=0 && degree<22.5))
        {return South;}
        else if(degree>=22.5 && degree<=67.5){return SouthWest;}
        else if(degree>67.5 && degree<112.5){return West;}
        else if(degree>=112.5 && degree<=157.5){return NorthWest;} 
        else if(degree>157.5 && degree<202.5){return North;}
        else if(degree>=202.5 && degree<=247.5){return NorthEast;}
        else if(degree>247.5 && degree<292.5){return East;}
        else if(degree>=292.5 && degree<=337.5){return SouthEast;}
    }

    return (
        <>

            {props.error && <small>Por favor ingrese una ciudad válida.</small>}

            {!props.loading && <p>Cargando...</p>}


            {(props.loading && !props.error) && (props.response !== {} && props.tab === 0) ?
                <div>
                    <h1 className={props.classes.title}>{props.location[0].name + ', ' + props.location[0].country}</h1>
                    <Grid container spacing={5} row='true' justify='center' align='center'>
                        <Grid item xs={12} sm={12} md={4}>
                            <Card className={props.classes.root}>
                                <CardActionArea>
                                    <CardMedia
                                        alt="Weather icon"
                                        height="140"
                                        title="Weather icon"
                                    >
                                        <WeatherIcon name={props.response.current.weather[0].icon} className={props.classes.media}/>                        
                                    </CardMedia>    
                                    <CardContent>
                                        <Grid container spacing={1} justify='center' align='center' column='true'>
                                            <Grid item xs={8}>
                                                <h2 className={props.classes.temp}>{Math.round(props.response.current.temp)}&deg;</h2>
                                                <p className={props.classes.text}>Sensación de <strong>{Math.round(props.response.current.feels_like)}&deg;</strong></p>
                                                <p className={props.classes.text}>Humedad <strong>{Math.round(props.response.current.humidity)}%</strong></p>  
                                            </Grid>
                                            <Grid item xs={4}>
                                                <img src={toIcon(props.response.current.wind_deg)} alt='flecha viento' className={props.classes.imageWind}></img>
                                                <p className={props.classes.text} style={{margin: 0}}>{Math.round(props.response.current.wind_speed)}m/s</p>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <p className={props.classes.text}><strong>{props.response.current.weather[0].description.slice(0, 1).toUpperCase() + props.response.current.weather[0].description.slice(1, )}.</strong></p>
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={12} md={8} className={props.classes.papers}>
                            <CustomScrollbars autoHide={false} className={props.classes.scrollbar}>
                                <div className={props.classes.pageContainer}>
                                    {
                                    [...Array(24)].map((x, i) =>
                                        <Paper elevation={3} className={props.classes.paper} key={i}>
                                            <Grid container spacing={1} column='true' justify='center' align='center'>
                                                <Grid item xs={12}>
                                                    <p className={props.classes.text}>{new Date((props.response.hourly[i+1].dt + props.response.timezone_offset) * 1000).getUTCHours().toString()}:00</p>
                                                </Grid>
                                                <Grid item xs={8}>
                                                    <WeatherIcon name={props.response.hourly[i+1].weather[0].icon} className={props.classes.media}/>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <p className={props.classes.text}>P. lluvia: <strong>{Math.round(props.response.hourly[i+1].pop * 100)}%</strong></p>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <h3 className={props.classes.temp}>{Math.round(props.response.hourly[i+1].temp)}&deg;</h3>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <img src={toIcon(props.response.hourly[i+1].wind_deg)} alt='flecha viento' className={props.classes.imageWindHourly}></img>
                                                    <p className={props.classes.text}>{Math.round(props.response.hourly[i+1].wind_speed)}m/s</p>
                                                </Grid>
                                            </Grid>
                                        </Paper>
                                    )}
                                </div>
                            </CustomScrollbars>
                            <div className={props.classes.displayMobile}>
                                <Grid container spacing={1} column='true' justify='center' align='center'>
                                    <Grid item xs={6}>
                                        <Paper elevation={3} className={props.classes.paper}>
                                            <Grid container spacing={1} column='true' justify='center' align='center'>
                                                <Grid item xs={12}>
                                                    <p className={props.classes.text}>{new Date((props.response.hourly[(activeStep+1)*2-1].dt + props.response.timezone_offset) * 1000).getUTCHours().toString()}:00</p>
                                                </Grid>
                                                <Grid item xs={8}>
                                                    <WeatherIcon name={props.response.hourly[(activeStep+1)*2-1].weather[0].icon} className={props.classes.media}/>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <p className={props.classes.text}>P. lluvia: <strong>{Math.round(props.response.hourly[(activeStep+1)*2-1].pop * 100)}%</strong></p>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <h3 className={props.classes.temp}>{Math.round(props.response.hourly[(activeStep+1)*2-1].temp)}&deg;</h3>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <img src={toIcon(props.response.hourly[(activeStep+1)*2-1].wind_deg)} alt='flecha viento' className={props.classes.imageWindHourly}></img>
                                                    <p className={props.classes.text}>{Math.round(props.response.hourly[(activeStep+1)*2-1].wind_speed)}m/s</p>
                                                </Grid>
                                            </Grid>
                                        </Paper>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Paper elevation={3} className={props.classes.paper}>
                                            <Grid container spacing={1} column='true' justify='center' align='center'>
                                                <Grid item xs={12}>
                                                    <p className={props.classes.text}>{new Date((props.response.hourly[(activeStep+1)*2].dt + props.response.timezone_offset) * 1000).getUTCHours().toString()}:00</p>
                                                </Grid>
                                                <Grid item xs={8}>
                                                    <WeatherIcon name={props.response.hourly[(activeStep+1)*2].weather[0].icon} className={props.classes.media}/>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <p className={props.classes.text}>P. lluvia: <strong>{Math.round(props.response.hourly[(activeStep+1)*2].pop * 100)}%</strong></p>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <h3 className={props.classes.temp}>{Math.round(props.response.hourly[(activeStep+1)*2].temp)}&deg;</h3>
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <img src={toIcon(props.response.hourly[(activeStep+1)*2].wind_deg)} alt='flecha viento' className={props.classes.imageWindHourly}></img>
                                                    <p className={props.classes.text}>{Math.round(props.response.hourly[(activeStep+1)*2].wind_speed)}m/s</p>
                                                </Grid>
                                            </Grid>
                                        </Paper>
                                    </Grid>    
                                </Grid>
                                <MobileStepper
                                    variant="progress"
                                    steps={24}
                                    position="static"
                                    activeStep={activeStep}
                                    className={props.classes.stepper}
                                    nextButton={
                                        <Button size="small" onClick={handleNext} disabled={activeStep === 23}>
                                            <KeyboardArrowRight />
                                        </Button>
                                    }
                                    backButton={
                                        <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
                                            <KeyboardArrowLeft />
                                        </Button>
                                    }
                                    />
                            </div>
                        </Grid>
                    </Grid>
                </div>
            : null
            }
            {(props.loading && !props.error) && (props.response !== {} && props.tab === 1) ?
                <div>
                    <h1 className={props.classes.title}>{props.location[0].name + ', ' + props.location[0].country}</h1>
                    <Grid container spacing={5} row='true' justify='center' align='center'>
                        {
                        [...Array(6)].map((x, i) =>
                        <Grid item xs={12} sm={6} md={4} lg={2} key={i} className={props.classes.displayDesktop}>
                            <Card className={props.classes.root}>
                                <CardActionArea>
                                    <CardMedia
                                        alt="Weather icon"
                                        height="140"
                                        title="Weather icon"
                                    >
                                        <WeatherIcon name={props.response.daily[i+1].weather[0].icon} className={props.classes.media}/>                        
                                    </CardMedia>    
                                    <CardContent>
                                        <Grid container spacing={1} justify='center' align='center' column='true'>
                                            <Grid item xs={12}>
                                                <h2 className={props.classes.title}>{weekday[new Date((props.response.daily[i+1].dt + props.response.timezone_offset) * 1000 ).getUTCDay()]}</h2>
                                            </Grid>
                                            <Grid item xs={9}>
                                                <h2 className={props.classes.tempMax}>{Math.round(props.response.daily[i+1].temp.max)}&deg; <span className={props.classes.tempMin}>{Math.round(props.response.daily[i+1].temp.min)}&deg;</span></h2>
                                                <p className={props.classes.text}  style={{margin: 0,}}>P. lluvia: <strong>{Math.round(props.response.daily[i+1].pop * 100)}%</strong></p>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <img src={toIcon(props.response.daily[i+1].wind_deg)} alt='flecha viento' className={props.classes.imageWind}></img>
                                                <p className={props.classes.text} style={{margin: 0, whiteSpace: 'nowrap'}}>{Math.round(props.response.daily[i+1].wind_speed)}m/s</p>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <p className={props.classes.textDescription}>{props.response.daily[i+1].weather[0].description.slice(0, 1).toUpperCase() + props.response.daily[i+1].weather[0].description.slice(1, )}.</p>
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </Grid>
                        )}
                        <Grid item xs={12} className={props.classes.displayMobile}>
                            <Card className={props.classes.root}>
                                <CardActionArea>
                                    <CardMedia
                                        alt="Weather icon"
                                        height="140"
                                        title="Weather icon"
                                    >
                                        <WeatherIcon name={props.response.daily[activeStep+1].weather[0].icon} className={props.classes.media}/>                        
                                    </CardMedia>    
                                    <CardContent>
                                        <Grid container spacing={1} justify='center' align='center' column='true'>
                                            <Grid item xs={12}>
                                                <h2 className={props.classes.title}>{weekday[new Date((props.response.daily[activeStep+1].dt + props.response.timezone_offset) * 1000 ).getUTCDay()]}</h2>
                                            </Grid>
                                            <Grid item xs={9}>
                                                <h2 className={props.classes.tempMax}>{Math.round(props.response.daily[activeStep+1].temp.max)}&deg; <span className={props.classes.tempMin}>{Math.round(props.response.daily[activeStep+1].temp.min)}&deg;</span></h2>
                                                <p className={props.classes.text}  style={{margin: 0,}}>P. lluvia: <strong>{Math.round(props.response.daily[activeStep+1].pop * 100)}%</strong></p>
                                            </Grid>
                                            <Grid item xs={3}>
                                                <img src={toIcon(props.response.daily[activeStep+1].wind_deg)} alt='flecha viento' className={props.classes.imageWind}></img>
                                                <p className={props.classes.text} style={{margin: 0, whiteSpace: 'nowrap'}}>{Math.round(props.response.daily[activeStep+1].wind_speed)}m/s</p>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <p className={props.classes.textDescription}>{props.response.daily[activeStep+1].weather[0].description.slice(0, 1).toUpperCase() + props.response.daily[activeStep+1].weather[0].description.slice(1, )}.</p>
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                            <MobileStepper
                                    variant="dots"
                                    steps={6}
                                    position="static"
                                    activeStep={activeStep}
                                    className={props.classes.stepper}
                                    nextButton={
                                        <Button size="small" onClick={handleNext} disabled={activeStep === 5}>
                                            <KeyboardArrowRight />
                                        </Button>
                                    }
                                    backButton={
                                        <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
                                            <KeyboardArrowLeft />
                                        </Button>
                                    }
                                    />   
                        </Grid>
                    </Grid>
                </div>
            : null
            }
        </>
    )
}

export default withStyles(styles)(Condiciones);