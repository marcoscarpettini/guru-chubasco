import React from 'react';
import { Link } from 'gatsby';
import { withStyles } from '@material-ui/styles';
import { AppBar, Toolbar } from '@material-ui/core';

import styles from './style';
import AvatarPic from '../../images/rain.png';

function Header(props) {
    const {classes} = props;
    
    return (
        <div>
            <AppBar>
                <Toolbar>
                    <Link to='/' className={classes.logo} edge="start">
                        <div className={classes.containerAvatar}>
                            <img src={AvatarPic} className={classes.avatar} alt='avatar' />
                        </div>
                    </Link>
                    <h3 className={classes.title}>Guru Chubasco</h3>
                </Toolbar>
            </AppBar>
        </div>
    )
};

export default withStyles(styles)(Header);