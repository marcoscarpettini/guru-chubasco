const styles = () => ({
    logo: {
        border: 'none',
        textDecoration: 'none',
        marginRight: '.5rem'
    },
    containerAvatar: {
        borderRadius: '50%',
        overflow: 'hidden',
        border: `.2rem solid white`,
        boxShadow: `0 0 0 .2rem rgba(0,0,0,.1)`,
        height: '2.75rem',
        width: '2.75rem',
    },
    avatar: {
        width: '100%',
    },
    title: {
        flexGrow: 1,
        justifySelf: 'center',
        marginTop: 'auto',
        marginBottom: 'auto',
        color: 'white',
    },
});

export default styles;