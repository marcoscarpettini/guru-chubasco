const styles  = () => ({
    '@global': {
      html: {
        overflow: 'hidden !important',
      },
    },
    container: {
      minHeight: '100vh',
      overflow: 'hidden !important',
    },
    pageContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      margin: '0 auto',
      minHeight: '100%',
      padding: '1rem 3rem',
      transition: 'filter .5s, opacity .5s',
      boxSizing: 'border-box',
    },
    content: {
      margin: 'auto',
      maxWidth: 'md',
      width: '100%',
    },
  });
  
export default styles;