import React from 'react';
import { withStyles } from '@material-ui/styles';
import { Scrollbars } from 'react-custom-scrollbars';
import Fade from '@material-ui/core/Fade';

import styles from './style';
import { Header, Footer } from '../';

function Layout(props) {
  const { classes, children } = props;

  const renderThumb = ({ style, ...props }) => {
    const thumbStyle = {
        borderRadius: 6,
        backgroundColor: '#0a4399',
    };
    return <div style={{ ...style, ...thumbStyle }} {...props} />;
  };

  const CustomScrollbars = props => (
    <Scrollbars
        renderThumbHorizontal={renderThumb}
        renderThumbVertical={renderThumb}
        {...props}
    />
  );

  return (
    <CustomScrollbars
      autoHide
      autoHideTimeout={1000}
      autoHideDuration={200}
      className={classes.container}>

      <div className={classes.pageContainer}>

        <Header />
        <Fade in mountOnEnter unmountOnExit>
          <main className={classes.content}> { children } </main>
        </Fade>
            
      </div>
      <Footer />
    </CustomScrollbars>
)};
  
export default withStyles(styles)(Layout);