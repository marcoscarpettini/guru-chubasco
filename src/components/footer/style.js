const styles = () => ({
    footer: {
        backgroundColor: '#333',
        color: '#fff',
        padding:' 2rem 0',
        textAlign: 'center',
    },
    container: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    iconos: {
        color: '#fff',
        fontSize: '3rem',
        width: '5rem',
        height: '5rem',
        margin: '1rem',
        textDecoration: 'none',
    },
    hr: {
        marginTop: '1rem',
        marginBottom: '1rem',
        border: 0,
        width: '50%',
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: 'grey',
    },
    firma: {
        color: 'grey',
        fontSize: '1rem',
    }
});

export default styles;