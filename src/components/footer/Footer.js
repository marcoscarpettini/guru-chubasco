import React from 'react';
import { Container } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fab } from '@fortawesome/free-brands-svg-icons';

import styles from './style';
import links from './content';

library.add(fab);

function Footer(props) {
    const {classes} = props;
    
    return (
        <footer className={classes.footer}>
            <Container>
                <div className={classes.container}>
                    {links.map((link, i) => (
                        <a key={i} target='_blank' rel="noreferrer" href={link.url} className={classes.iconos}>
                            <FontAwesomeIcon icon={['fab', link.name]} />
                        </a>
                    ))}
                </div>
                <hr className={classes.hr}/>
                <p className={classes.firma}>
                    © 2021 - Página creada por Marco Scarpettini.
                </p>
            </Container>
        </footer>
    );
}

export default withStyles(styles)(Footer);