import React from "react";
import { Layout, Clima, SEO } from '../components'

const IndexPage = () => (
    <Layout>
      <SEO title="Inicio" />
      <Clima />
    </Layout>
);

export default IndexPage;
