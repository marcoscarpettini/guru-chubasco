import React from "react";
import { Link } from "gatsby";
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import styles from '../files/style404';

const ColorButton = withStyles(() => ({
  root: {
    color: '#ffffff',
    backgroundColor: '#0a4399',
    '&:hover': {
      backgroundColor: '#0a3575',
    },
  }, 
}))(Button);

// styles
const body = {

}

const contenido = {
  maxWidht: '600px',
  textAlign: 'center',
}

const pagina = {
  position: 'absolute',
  top: '10%',
  left: '17%',
  right: '17%',
  bottom: '10%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: '#fff',
  boxShadow: '0px 5px 10px rgba(0, 0, 0, 0.3)',
  maxWidht: '600px',
  textAlign: 'center',
}

const numero = {
  fontSize: '18vw',
  lineHeight: '1em',
  marginTop: '2rem',
}

const titulo = {
  marginBottom: '20px',
  color: '#000',
  maxWidht: '600px',
  fontSize: '2em',
  marginTop: '2rem',
}

const texto = {
  fontSize: '1.2em',
  color: '#0d0d0d',
  marginRight: '1rem',
  marginLeft: '1rem',
  marginBottom: '1rem',
}

const boton = {
  textDecoration: 'none',
}
// markup
const NotFoundPage = () => {
  return (
    <div style={pagina}>
      <div style={contenido}>
        <h1 style={numero}>404</h1>
        <h4 style={titulo}>Página no encontrada</h4>
        <p style={texto}>La página que estas buscando no existe. Si pensás que algo no esta funcionando como deberia, por favor notifica el problema.</p>
        <Link to='/' style={boton}><ColorButton>Volver a inicio</ColorButton></Link>
      </div>
    </div>
  )
}

export default withStyles(styles)(NotFoundPage)
