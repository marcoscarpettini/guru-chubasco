const styles  = () => ({
    '@global' :{
        html: {
            overflowY: 'hidden !important',
        },
        body: {
            backgroundImage: 'linear-gradient(-45deg, #0a4399, #0a3575)',
            height: '100vh',
        }
    }
});

export default styles;