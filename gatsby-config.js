module.exports = {
  siteMetadata: {
    title: "Guru Chubasco",
  },
  plugins: [
    `gatsby-plugin-netlify`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Guru Chubasco`,
        short_name: `Guru Chubasco`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#0a4399`,
        display: `standalone`,
        description: `Esta es una página web de pronostico del tiempo y visualización de las distintas variables metereológicas de cualquier lugar del mundo.`,
        lang: `es`,
        icon: `src/images/rain.png`, // This path is relative to the root of the site.
        icons: [
          {
            src: `/icons/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/icons/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
          {
            src: '/icons/maskable_iconx192',
            sizes: '192x192',
            type: 'image/png',
            purpose: 'any maskable',
          },
          {
            src: '/icons/maskable_iconx512',
            sizes: '512x512',
            type: 'image/png',
            purpose: 'any maskable',
          }
        ], // Add or remove icon sizes as desired
      },
    },
    {  
      resolve: `gatsby-plugin-offline`,
      options: {
        precachePages: [`/index/`, `/404/`],
      },
    },
  ],
};
